# Java 8 features code @okayjava-git

# Instructions to create a local git repo and pull the code from master branch.

C:\>mkdir Gitlab-projects

C:\>cd Gitlab-projects

C:\Gitlab-projects>mkdir java-8-features-code

C:\Gitlab-projects>cd java-8-features-code

C:\Gitlab-projects\java-8-features-code>git init
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint:
hint:   git config --global init.defaultBranch <name>
hint:
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint:
hint:   git branch -m <name>
Initialized empty Git repository in C:/Gitlab-projects/java-8-features-code/.git/

C:\Gitlab-projects\java-8-features-code>git remote add origin https://gitlab.com/java-8-features/java-8-features-code.git

C:\Gitlab-projects\java-8-features-code>git remote -v
origin  https://gitlab.com/java-8-features/java-8-features-code.git (fetch)
origin  https://gitlab.com/java-8-features/java-8-features-code.git (push)

C:\Gitlab-projects\java-8-features-code>git pull origin master
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 335 bytes | 11.00 KiB/s, done.
From https://gitlab.com/java-8-features/java-8-features-code
 * branch            master     -> FETCH_HEAD
 * [new branch]      master     -> origin/master

C:\Gitlab-projects\java-8-features-code>


# Instructions to make a pull request (sync local repo with master)
C:\Gitlab-projects\java-8-features-code>git status
On branch master
nothing to commit, working tree clean

C:\Gitlab-projects\java-8-features-code>git pull origin master
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 874 bytes | 31.00 KiB/s, done.
From https://gitlab.com/java-8-features/java-8-features-code
 * branch            master     -> FETCH_HEAD
   bbe162a..248423b  master     -> origin/master
Updating bbe162a..248423b
Fast-forward
 README.md | 50 +++++++++++++++++++++++++++++++++++++++++---------
 1 file changed, 41 insertions(+), 9 deletions(-)

C:\Gitlab-projects\java-8-features-code>git status
On branch master
nothing to commit, working tree clean

C:\Gitlab-projects\java-8-features-code>

# Commit code changes in local repository and push to Master/Remote repository
Note: I have already copied code files in local git project 'java-8-features-code'
C:\Gitlab-projects\java-8-features-code>git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        src/

nothing added to commit but untracked files present (use "git add" to track)

C:\Gitlab-projects\java-8-features-code>git add *

C:\Gitlab-projects\java-8-features-code>git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   src/com/java/features/AlphaNumericPassword.java
        new file:   src/com/java/features/BuiltInFunctionalInterfaces.java
        new file:   src/com/java/features/DeafultMethodsInterface.java
        new file:   src/com/java/features/DefaultMethodsInheritance.java
        new file:   src/com/java/features/DummyPassword.java
        new file:   src/com/java/features/ExistingFunctionalIntefaces.java
        new file:   src/com/java/features/FunctionInterface.java
        new file:   src/com/java/features/FunctionInterfaceInheritance.java
        new file:   src/com/java/features/FunctionalInterfaceExample.java
        new file:   src/com/java/features/IntefaceExample.java
        new file:   src/com/java/features/LambdaExp.java
        new file:   src/com/java/features/LambdaExpBody.java
        new file:   src/com/java/features/LambdaExpExmple.java
        new file:   src/com/java/features/LambdaIntExp.java
        new file:   src/com/java/features/LambdaVarScope.java
        new file:   src/com/java/features/MasterInterface.java
        new file:   src/com/java/features/NumericPassword.java
        new file:   src/com/java/features/PlayerComparatorFunctionalInterface.java
        new file:   src/com/java/features/PredicateFunctionalInterface.java
        new file:   src/com/java/features/PredicateInterface.java
        new file:   src/com/java/features/SpecialCharsPassword.java
        new file:   src/com/java/features/StaticMethodsInterface.java
        new file:   src/com/java/features/TextPassword.java


C:\Gitlab-projects\java-8-features-code>git commit -m "initial commit"
[master e3bd330] initial commit
 23 files changed, 877 insertions(+)
 create mode 100644 src/com/java/features/AlphaNumericPassword.java
 create mode 100644 src/com/java/features/BuiltInFunctionalInterfaces.java
 create mode 100644 src/com/java/features/DeafultMethodsInterface.java
 create mode 100644 src/com/java/features/DefaultMethodsInheritance.java
 create mode 100644 src/com/java/features/DummyPassword.java
 create mode 100644 src/com/java/features/ExistingFunctionalIntefaces.java
 create mode 100644 src/com/java/features/FunctionInterface.java
 create mode 100644 src/com/java/features/FunctionInterfaceInheritance.java
 create mode 100644 src/com/java/features/FunctionalInterfaceExample.java
 create mode 100644 src/com/java/features/IntefaceExample.java
 create mode 100644 src/com/java/features/LambdaExp.java
 create mode 100644 src/com/java/features/LambdaExpBody.java
 create mode 100644 src/com/java/features/LambdaExpExmple.java
 create mode 100644 src/com/java/features/LambdaIntExp.java
 create mode 100644 src/com/java/features/LambdaVarScope.java
 create mode 100644 src/com/java/features/MasterInterface.java
 create mode 100644 src/com/java/features/NumericPassword.java
 create mode 100644 src/com/java/features/PlayerComparatorFunctionalInterface.java
 create mode 100644 src/com/java/features/PredicateFunctionalInterface.java
 create mode 100644 src/com/java/features/PredicateInterface.java
 create mode 100644 src/com/java/features/SpecialCharsPassword.java
 create mode 100644 src/com/java/features/StaticMethodsInterface.java
 create mode 100644 src/com/java/features/TextPassword.java

C:\Gitlab-projects\java-8-features-code>git push origin master
Username for 'https://gitlab.com': okayjava-git
Password for 'https://okayjava-git@gitlab.com':
Enumerating objects: 30, done.
Counting objects: 100% (30/30), done.
Delta compression using up to 4 threads
Compressing objects: 100% (26/26), done.
Writing objects: 100% (29/29), 8.77 KiB | 1.10 MiB/s, done.
Total 29 (delta 2), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/java-8-features/java-8-features-code.git
   9ebfddb..e3bd330  master -> master

C:\Gitlab-projects\java-8-features-code>

