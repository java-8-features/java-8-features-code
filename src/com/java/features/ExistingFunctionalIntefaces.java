package com.java.features;

public class ExistingFunctionalIntefaces implements Runnable{

	public static void main(String[] args) {
		ExistingFunctionalIntefaces ref = new ExistingFunctionalIntefaces();
		Thread th = new Thread(ref);
		th.start();
		
	}

	@Override
	public void run() {
		System.out.println("Functional interface before java 8.");
	}
}
