package com.java.features;

public class SpecialCharsPassword implements MasterInterface {

	public static void main(String[] args) {
		
		SpecialCharsPassword obj = new SpecialCharsPassword();
		obj.validatePassword("Abcds@1234");
		obj.validateUserName("okayjava@youtube");
		
		
	}
	
	@Override
	public boolean validatePassword(String input) {
		System.out.println("Password should have special chars");
		return false;
	}

}
