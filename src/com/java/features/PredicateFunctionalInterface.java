package com.java.features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateFunctionalInterface {

	public static void main(String[] args) {

		Employee e1 = new Employee(1,23,"M","Ricky","luther");
        Employee e2 = new Employee(2,22,"F","Rosie","Taylor");
        Employee e3 = new Employee(3,33,"M","Jayden","west");
        Employee e4 = new Employee(4,21,"M","Shane","Lowman");
        Employee e5 = new Employee(5,19,"F","Cristine","Josie");
        Employee e6 = new Employee(6,25,"M","David","Feezor");
        Employee e7 = new Employee(7,38,"F","Scarlet","Ellie");
        Employee e8 = new Employee(8,41,"M","Russell","Gussin");
        Employee e9 = new Employee(9,35,"F","Rakul","Singh");
        Employee e10 = new Employee(10,45,"M","Aiden","Taylor");
        
        List<Employee> employees = new ArrayList<Employee>();
        employees.addAll(Arrays.asList(new Employee[]{e1,e2,e3,e4,e5,e6,e7,e8,e9,e10}));
        
        // print young emp age : < 21 < 35 
        Predicate<Employee> isYoungEmp = (e) -> (e.getAge()> 21 && e.getAge() < 35);
        
        Predicate<Employee> isFemale = (e) -> e.getGender().equalsIgnoreCase("F");
        
        // find out morethan 40
        int ageMorethan = 40;
        Predicate<Employee> isMorethan = (e) -> e.getAge()> ageMorethan;
        
        BiPredicate<Employee,Integer> biPred = (e, age) -> e.getAge() > age;
        
        for(Employee e : employees) {
        	/*
        	// print young employee
        	if(isYoungEmp.test(e)) {
        		System.out.println(e);
        	}
        
        	// female employee
        	if(isFemale.test(e)) {
        		System.out.println(e);
        	}
        	
        	
        	if(isMorethan.test(e)) {
        		System.out.println(e);
        	}
        	
        	
        	// can isYoungEmp  female emp
        	if(isYoungEmp.and(isFemale).test(e)) {
        		System.out.println(e);
        	}
        	
        	
        	if(biPred.test(e, 20)) {
        		System.out.println(e);
        	}
        	
        	
        	if(biPred.test(e, 35)) {
        		System.out.println(e);
        	}
        	
        	*/
        	
        	if(isMorethan.negate().test(e)) {
        		System.out.println(e);
        	}
        	
        }
        
        
        
	}

}


class Employee {
	     
	   private Integer id;
	   private Integer age;
	   private String gender;
	   private String firstName;
	   private String lastName;
	 
	   public Employee(Integer id, Integer age, String gender, String fName, String lName){
	       this.id = id;
	       this.age = age;
	       this.gender = gender;
	       this.firstName = fName;
	       this.lastName = lName;
	   } 
	   
	   
	   public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Override
	    public String toString() {
	        return this.id.toString()+" Name - "+ this.firstName.toString() +" Age - "+this.age.toString(); 
	    }
	}