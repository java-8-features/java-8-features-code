package com.java.features;

@FunctionalInterface // optional annotation
interface MyFirstInteface {
	
	public void first(); // abstract function
	
	default void second() {
		System.out.println("I am second.");
	}
	
	static void third() {
		System.out.println("I am third");
	}
	
}

public class FunctionalInterfaceExample implements MyFirstInteface {

	public static void main(String[] args) {
		FunctionalInterfaceExample obj = new FunctionalInterfaceExample();
		
		obj.first();
		
	}
	
	@Override
	public void first() {
	System.out.println("My first functional interface");
		
	}

}
