package com.java.features;

@FunctionalInterface
interface AddInterface{
	
	public int squareit(int x);
	
}

public class LambdaIntExp {
	
	public static void main(String[] args) {
		
		AddInterface ref =  x -> { return x*x; };
		
		System.out.println(ref.squareit(5));
		
		
	}

	public int squareit(int x) {
		return x*x;
	}
}
