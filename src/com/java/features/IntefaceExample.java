package com.java.features;

interface FirstInterface{
	
	public void sayHello(); // abstract function
	
	public void sayGoodNight();// abstract
	
}

public class IntefaceExample implements FirstInterface {

	public static void main(String[] args) {
		
		IntefaceExample obj = new IntefaceExample();
		obj.sayHello();
		
		obj.sayGoodNight();
	}
	
	@Override
	public void sayHello() {
		System.out.println("Hello ");
		
	}

	@Override
	public void sayGoodNight() {
		System.out.println("Good night");
		
	}

}
