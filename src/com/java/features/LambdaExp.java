package com.java.features;

@FunctionalInterface
interface LearningLambdaExp{
	public void learn(); // abstract
}

/*
public class LambdaExp implements LearningLambdaExp{

	public static void main(String[] args) {
		LambdaExp obj = new LambdaExp();
		obj.learn();
	}

	@Override
	public void learn() {
		System.out.println("Learning lambda exp ");
		
	}

}
*/

public class LambdaExp {
	public static void main(String[] args) {
		
		LearningLambdaExp ref = () -> { System.out.println("Learning lambda exp ");};
		
		ref.learn();
		
		
	}
}
