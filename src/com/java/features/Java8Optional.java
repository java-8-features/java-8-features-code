package com.java.features;

import java.util.Date;
import java.util.Optional;

public class Java8Optional {

	public static void main(String[] args) {

		// Create an optional object
		Optional<String> myStr1 = Optional.empty();
		System.out.println(myStr1);
		
		// Optional.of
		Optional<String> myStr2 = Optional.of("First");
		System.out.println(myStr2.get());
		
		// Optional.ofNullable
		Optional<String> myStr3 = Optional.ofNullable(null);
		System.out.println(myStr3);
		/*
		if(myStr3.isPresent()) {
			System.out.println("myStr3 is not null " + myStr3.get());
		}else {
			System.out.println("myStr3 is not null");
		}*/
		
		// ifPresent
		myStr3.ifPresent(str -> System.out.println(str.toUpperCase()));
		
		// orElse
		
		System.out.println(myStr1.orElse("okayjava"));
		
		String country = "Canada";
		Optional<String> c = Optional.ofNullable(country);
		System.out.println("orElse= "+ c.orElse("India"));
		
		
		String country2 = "Canada";
		Optional<String> c2 = Optional.ofNullable(country2);
		System.out.println("orElseGet = "+ c2.orElseGet(() -> "India"));
		
		
		// orElseGet
		Optional<Date> today = Optional.empty();
		System.out.println(today.orElseGet(() -> new Date()));

		// orElseThrow
		//Optional<Integer> item = Optional.empty();
		Optional<Integer> item = Optional.of(1);
		System.out.println("orElseThrow = "+item.orElseThrow(IllegalArgumentException :: new));
		
		Optional<Integer> age = Optional.of(20);
		System.out.println(".filter = "  +age.filter(n -> n>18).get());
		
		// map
		Optional<Integer> price = Optional.ofNullable(20);
		//System.out.println("map = " + price.map(p -> p*p));
		
		Optional<Integer> prc = price.map(p -> p*p);
		if(prc.isPresent()) {
			System.out.println(prc.get());
		}
		
		
	}

}
