package com.java.features;

interface RBCBank{
	
	default boolean depositMoneyInRBC(Double amount) {
		System.out.println("Deposit in RBC >> "  + amount);
		return true;
	}
	
	default Double transferMoney(Double amount) {
		System.out.println("RBC Send money >> " + amount);
		return amount;
	}
	
}
 
interface CIBCBank {
	
	default boolean depositMoneyInCIBC(Double amount) {
		System.out.println("Deposit in CIBC >> "  + amount);
		return true;
	}
	
	default Double transferMoney(Double amount) {
		System.out.println("CIBC Send money >> " + amount);
		return amount;
	}
	
}


public class DefaultMethodsInheritance implements RBCBank, CIBCBank{

	public static void main(String[] args) {
		DefaultMethodsInheritance obj = new DefaultMethodsInheritance();
		obj.depositMoneyInCIBC(10000.00);
		obj.depositMoneyInRBC(5000.00);
		obj.transferMoney(3000.50);
	
	}

	@Override
	public Double transferMoney(Double amount) {
		return RBCBank.super.transferMoney(amount);
	}
	
	
}
