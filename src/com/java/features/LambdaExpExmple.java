package com.java.features;

@FunctionalInterface
interface LambdaInterface{
	//public int addInt(int a, int b); // abstract
	
	public int getLength(String str);
}

public class LambdaExpExmple {

	public static void main(String[] args) {
		
		/*
		LambdaInterface ref = (a, b) -> {
			return a+b;
		};
		
		System.out.println(ref.addInt(5, 10));
		*/
		
		LambdaInterface ref = str -> {return str.length();};
		
		int len = ref.getLength("okayjava");
		System.out.println("len = " + len);
		
	}
	
	
}
