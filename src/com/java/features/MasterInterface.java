package com.java.features;

public interface MasterInterface {

	public boolean validatePassword(String input);
	
	// add new function/method
	default public boolean validateUserName(String username) {
		System.out.println("Write your code to validate the username");
		return true;
	}
	
}
