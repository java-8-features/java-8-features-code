package com.java.features;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerFunctionalInterface {

	public static void main(String[] args) {

		
		Consumer<String> c1 = (str) -> System.out.println(str);
	//	c1.accept("okayjava");
		
		Consumer<String> cUpperCase = (str) -> System.out.println(str.toUpperCase());
	//	cUpperCase.accept("okayjava");
		
		Consumer<String> cReverse = (s) -> {
			StringBuilder sb = new StringBuilder(s);
			System.out.println(sb.reverse());
		};
					
		c1.andThen(cUpperCase).andThen(cReverse).accept("okayjava");
		
		Consumer<Integer> squareit = (val) -> System.out.println("val = " +(val*val));
		/*
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		
		for(int i : list) {
			squareit.accept(i);
		}
		*/
		//list.stream().forEach(squareit);
		
		List<Integer> empSal = Arrays.asList(1000,2000,3000,4000,500,6000,7000,8000,9000,10000);
		BiConsumer<Integer, Integer> biCo = (sal, bonus) -> System.out.println(sal+bonus);
		System.out.println("BiConsumer example >>>>>>>>>>>>");
		for(int sal : empSal) {
			biCo.accept(sal, 500);
		}
		
	}

}
