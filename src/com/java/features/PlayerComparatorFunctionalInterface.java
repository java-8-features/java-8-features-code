package com.java.features;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PlayerComparatorFunctionalInterface {

	public static void main(String[] args) {
		
		List<Player> list = new ArrayList<Player>();
		list.add(new Player("Shewag", 110, 4341, 22, 40));
		list.add(new Player("Sachin", 250, 11324, 100, 125));
		list.add(new Player("Yuvraj", 105, 6533, 15, 46));
		list.add(new Player("Kohli", 75, 4003, 25, 60));
		list.add(new Player("Raina", 34, 2600, 12, 19));
		list.add(new Player("Rohit", 25, 1500, 5, 9));
		/*
		for(Player p : list) {
			System.out.println(p);
     	}
		*/
		Collections.sort(list, new PlayerComparator());
		for(Player p : list) {
			System.out.println(p);
     	}
		
   }

}

class PlayerComparator implements Comparator<Player> {

	@Override
	public int compare(Player p1, Player p2) {
		
		//return p1.name.compareTo(p2.name);
		
		if(p1.runsScored > p2.runsScored) {
			return 1;
		}else if(p1.runsScored < p2.runsScored) {
			return -1;
		}else {
			return 0; // equal
		}
		
		
	}
	
}

class Player{
	
	String name;
    int matchesPlayed;
    int runsScored;
    int numberOfCenturies;
    int numberOfHalfCenturies;
	
    public Player(String name, int matchesPlayed, int runsScored, int numberOfCenturies, int numberOfHalfCenturies) {
		this.name = name;
		this.matchesPlayed = matchesPlayed;
		this.runsScored = runsScored;
		this.numberOfCenturies = numberOfCenturies;
		this.numberOfHalfCenturies = numberOfHalfCenturies;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", matchesPlayed=" + matchesPlayed + ", runsScored=" + runsScored
				+ ", numberOfCenturies=" + numberOfCenturies + ", numberOfHalfCenturies=" + numberOfHalfCenturies + "]";
	}
    
    
}
