package com.java.features;

@FunctionalInterface
interface LambdaExpInterface01{
	public void print(String s1, String s2);
}

public class LambdaExpBody {

	public static void main(String[] args) {
		
		LambdaExpInterface01 ref = (String s1, String s2) -> {
			System.out.println("s1 =" + s1 );
			System.out.println("s2 = " + s2 );
		};
		
		ref.print("youtube", "okayjava");
	}

	/*
	public void print(String s1, String s2) {
		System.out.println("s1 =" + s1 + " >> s2 = " + s2 );
	}
	*/
}
