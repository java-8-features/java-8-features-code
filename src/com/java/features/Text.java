package com.java.features;

@FunctionalInterface
interface MsgInterface{
	Text createMsg(String s);
}

public class Text {

	Text(String str){
		System.out.println("Message " + str);
	}
	
	public static void main(String[] args) {

		MsgInterface ref = (s) -> new Text(s); // calling a param constructor
		ref.createMsg("okayjava");
		
		MsgInterface ref1 = Text :: new;
		ref1.createMsg("okayjava");
		
	}

}
