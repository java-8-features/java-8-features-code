package com.java.features;

@FunctionalInterface
interface AbcInterface{
	public int operationX(int x);
}

public class Java8MethodReference {

	public int cubeIt(int a) {
		return a*a*a;
	}
	
	public static void main(String[] args) {
		/*
		AbcInterface ref = (x) -> (x*x);
		System.out.println(ref.operationX(5));
	    */
		
		AbcInterface ref1 = Java8MethodReference :: squareIt;
		System.out.println(ref1.operationX(5));
		
		Java8MethodReference object = new Java8MethodReference();
		AbcInterface ref2 = object :: cubeIt;
		System.out.println(ref2.operationX(5));
		
		
		
	}
	
	public static int squareIt(int n) {
		return n*n;
	}

}
