package com.java.features;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;
import java.util.function.Supplier;

public class SupplierFunctionalInterface {

	public static void main(String[] args) {
		
		Supplier<Integer>  supplyInt = () -> new Random().nextInt(100);
		System.out.println(supplyInt.get());
		
		Supplier<LocalDate> date = () -> LocalDate.now();
		System.out.println("Date = " + date.get());
		
		Supplier<LocalTime> time = () -> LocalTime.now();
		System.out.println("time = " + time.get());
		
		Supplier<Integer>  supplyRandomInt = SupplierFunctionalInterface :: getRandomInt;
		System.out.println("supplyRandomInt = " + supplyRandomInt.get());
		
		
		IntSupplier intval = () -> 9;
		System.out.println("intval = " + intval.getAsInt());
		
		LongSupplier longVal = () -> 9L;
		System.out.println("longVal =" + longVal.getAsLong());
		
		DoubleSupplier doubleVal = () -> 10.0;
		System.out.println("doubleVal = " + doubleVal.getAsDouble());
		
		BooleanSupplier booleanVal = () -> true;
		System.out.println("booleanVal = " + booleanVal.getAsBoolean() );
		
		
		
	}
	
	public static Integer getRandomInt(){
		
		return new Random().nextInt(10);
	}
	
	
}
