package com.java.features;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class BuiltInFunctionalInterfaces {

	public static void main(String[] args) {

		
		Predicate<String> p = s -> (s!=null && s.length()>0);
		System.out.println("predicate =" + p.test(null));

		Predicate<String> pMinLength = s -> s.length()>4;
		System.out.println("pMinLength =" + pMinLength.test("okayjava"));
		
		Predicate<String> pMxLength = s -> s.length() < 10;
		System.out.println("pMxLength =" + pMxLength.test("okayjavaokay"));
		
		System.out.println("predicate joining AND =" + p.and(pMinLength).and(pMxLength).test("okayjava"));
		
		System.out.println("predicate joining OR =" + pMinLength.or(pMxLength).test("abc"));
		
		
		
		Function<String, Integer> f = (s) -> s.length();
		System.out.println("Function = " + f.apply("okayjava"));
		
		Function<String, Boolean> fn = s -> s!=null && s.length()>0;
		System.out.println("fn = " + fn.apply("okayjava"));
		
		Consumer<String> consumer = str -> System.out.println(str);
		consumer.accept("okayjava");
		
		Supplier<Date> supplier = () -> new Date();
		System.out.println(supplier.get());
		
		UnaryOperator<Integer> cubeX = x -> x*x*x;
		System.out.println("cubex = " + cubeX.apply(5));
		
		
	}

}
