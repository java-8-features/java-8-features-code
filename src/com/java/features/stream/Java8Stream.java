package com.java.features.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java8Stream {

	public static void main(String[] args) {

		// Stream pipeline flow
		// 1. source of data
		List<Integer> intList = Arrays.asList(1,2,3,4,5,6);
		System.out.println("Source : PRE = " + intList);
		
		// 2. Create/ convert source into a java 8 stream
		Stream<Integer> intStream =  intList.parallelStream();//stream(); // create a stream
		
		// 3. intermediate/non terminal
		Stream<Integer> filteredStream = intStream.filter(ele -> ele > 3);
		
		// 4. intermediate ops
		Stream<Integer> computedStream = filteredStream.map(n -> n*n).peek(e -> System.out.println("Filtered value: " + e));
		// 5. terminal ops
		//computedStream.forEach(a -> System.out.println(a));
		System.out.println("Computed result...");
		computedStream.forEachOrdered(System.out :: println);
		
		System.out.println("Source : Post = " + intList);
		
		System.out.println("one liner code ");
		intList.stream().filter(ele -> ele > 3).map(n -> n*n).forEach(System.out :: println);
		
		/*
		System.out.println("stream.peek");
		Stream.of("one", "two", "three", "four")
		  .filter(e -> e.length() > 3)
		  .peek(e -> System.out.println("Filtered value: " + e))
		  .map(String::toUpperCase)
		  .peek(e -> System.out.println("Mapped value: " + e))
		  .collect(Collectors.toList());
		*/
	}

}
