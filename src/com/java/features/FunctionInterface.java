package com.java.features;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionInterface {

	public static void main(String[] args) {

		Function<String, Integer> f1 = (s) -> s.length();
		System.out.println(f1.apply("okayjava"));
		
		Function<String, String> fStr = (s) -> s.toUpperCase();
		System.out.println(fStr.apply("okayjava"));
		
		Function<Integer, Integer> add = (x) -> x+3;
		int a = add.apply(2);
		System.out.println("add ="+a);
		
		Function<Integer, Integer> multiply = val -> val*2;
		int m = multiply.apply(a);
		System.out.println("multiply = " + m);
		
		System.out.println("function chaining .andThen= " + add.andThen(multiply).apply(2));
		
		System.out.println("function chaining .compose() = " + add.compose(multiply).apply(2));
		
		System.out.println("function chaining .andThen reverse = " + multiply.andThen(add).apply(2));
		
		
		// what is the diff b/w predicate and function
		
		Predicate<String> p = x -> x.length()>5;
		System.out.println("p = "+p.test("okayjava"));
		
		Function<String, Boolean> f = s -> s.length() > 5;
		System.out.println("f = " + f.apply("okayjava"));
		
		BiFunction<String, String, String> strCon = (s1, s2) -> s1+s2;
		System.out.println(strCon.apply("Okay ", " Java"));
		
		BiFunction<Integer, Integer, Boolean> compare = (i , j) -> i>j;
		System.out.println(compare.apply(25, 10));
		
		
		
	}

}
