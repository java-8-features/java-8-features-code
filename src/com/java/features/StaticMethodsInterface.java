package com.java.features;

interface SomeUtility{
	
	static boolean checkSpecialChars(String str) {
		System.out.println("checkSpecialChars " + str);
		if(str.contains("@")) {
			return true;
		}else {
			return false;
		}
	}
	
}

interface UtilityInterface extends SomeUtility{
	
	static public boolean isNUll(String input) {
		System.out.println("UtilityInterface isNUll > " + input);
		return input==null ? true : ("".equals(input) ? true : false);
	}
	
	static public int getLength(String str) {
		System.out.println("UtilityInterface getLength > " + str);
		if(!isNUll(str)) {
			return str.length();
		}else {
			return 0;
		}
	}
}

public class StaticMethodsInterface implements UtilityInterface{
	
	public static void main(String[] args) {
		boolean isNotNUll = UtilityInterface.isNUll(null);
		System.out.println("isNotNUll = " + isNotNUll);
		
		System.out.println("length = " + UtilityInterface.getLength(null));
	
		System.out.println("check @ " +SomeUtility.checkSpecialChars("okayjava@youtube"));
		/*
		StaticMethodsInterface obj = new StaticMethodsInterface();
		isNotNUll = obj.isNUll(null);
		System.out.println("isNotNUll = " + isNotNUll);
		*/
	}
    
	
	public boolean isNUll(String input) {
		System.out.println("Developer-B > isNUll > " + input);
		//return input==null ? true : false;
		
		//return "".equals(input) ? true : false;
		return input==null ? true : ("".equals(input) ? true : false);
	}

}
