package com.java.features;
//regular interface 
interface TopInteface{
	// marker interface > Seriablizable 
}

@FunctionalInterface
interface Printable extends TopInteface{
	public void print();
}

@FunctionalInterface
interface ChildInterface extends Printable {
	
	default void first() {
		
	}
	
	static void second() {
		
	}
	
}

public class FunctionInterfaceInheritance implements ChildInterface {

	@Override
	public void print() {
		System.out.println("I am printing..");
		
	}
	
	public static void main(String[] args) {
		FunctionInterfaceInheritance obj = new FunctionInterfaceInheritance();
		obj.print();
	}

}
