package com.java.features;

import java.util.function.Predicate;
/*
@FunctionalInterface
interface NullValueInterface{
	boolean checkNUll(String s);
}

@FunctionalInterface
interface StartsWithInterface{
	boolean startsWith(String s);
}

@FunctionalInterface
interface EndsWIthInterface{
	boolean endsWith(String s);
}
*/
public class PredicateInterface {

	public static void main(String[] args) {
		
		Predicate<String> p = (s) -> (s!=null && s.length()>0);
		System.out.println("predicate =" + p.test("okayjava"));
		
		Predicate<String> pStartsWith = s -> s.startsWith("o") || s.startsWith("O");
		System.out.println("pStartsWith =" + pStartsWith.test("okayjava"));
		
		Predicate<String> pEndsWith = s -> s.endsWith("a");
		System.out.println("pEndsWith =" + pEndsWith.test("okayjava"));
		
		System.out.println("predicate joining AND =" 
		                   + p.and(pStartsWith).and(pEndsWith).test(null));
		
		System.out.println("predicate joining OR =" + pStartsWith.or(pEndsWith).test("java"));
		
		System.out.println(pEndsWith.test("java"));
		System.out.println(pEndsWith.negate().test("java"));
		
	}

}
