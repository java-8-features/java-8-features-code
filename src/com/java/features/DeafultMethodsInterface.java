package com.java.features;

interface TestInterface{
	
	public void test();
	
	default public void dummyTest() {
		System.out.println("I am dummyTest function");
	}
	
	default public void dummyTrial() {
		System.out.println("I am dummyTrial");
	}
	
}


public class DeafultMethodsInterface implements TestInterface{

	public static void main(String[] args) {
		DeafultMethodsInterface obj = new DeafultMethodsInterface();
		obj.test();
		obj.dummyTest();
		obj.dummyTrial();
	}
	
	@Override
	public void test() {
	System.out.println("I am test function.");
		
	}
	
	public void dummyTest() {
		System.out.println("i am overriding dummyTest()");
	}

}
