package com.java.features;

@FunctionalInterface
interface InterfaceVar{
	public void varScope(); // abstract
}

public class LambdaVarScope {

	int eId; // instance var
	String eName; // instance var
	static String company = "Abc Ltd."; // static variable
	
	public static void main(String[] args) {
	
		// local variables > effectively final 
		int companyAge = 25; // local var // effectively final > consider var as final variable.
		String role = "Developer"; // local var
		
		LambdaVarScope obj = new LambdaVarScope();
		
		InterfaceVar ref = () -> {
			System.out.println("Lambda exp local variables..");
			//age = age + 1;
			//role = "Sr. Developer";
			System.out.println("Emp age = " + companyAge);
			System.out.println("Emp role = " + role);
			
			System.out.println("Lambda exp staic variable ");
			company = "XYZ Ltd.";
			System.out.println("Emp company = " + company);
			obj.eId = 110;
			obj.eName = "Rakul preet";
			System.out.println("Lambda exp instance variable ");
			System.out.println("Emp id = " + obj.eId);
			System.out.println("Emp eName = " + obj.eName);
			
		};
		
		ref.varScope();
		
		
	}

}
